import React, { Component } from "react";
import { Provider } from "react-redux";
import { createStore, applyMiddleware } from "redux";
import firebase from "firebase";
import reducers from "./reducers";
import ReduxThunk from "redux-thunk";
import Router from "./Router";

export default class App extends Component {
  componentWillMount() {
    const config = {
      apiKey: "AIzaSyCJJw3xvEM1MHS6EGpCshGn1ei7vMGrr7s",
      authDomain: "manager-d259d.firebaseapp.com",
      databaseURL: "https://manager-d259d.firebaseio.com",
      projectId: "manager-d259d",
      storageBucket: "manager-d259d.appspot.com",
      messagingSenderId: "871863229756"
    };
    firebase.initializeApp(config);
  }
  render() {
    const store = createStore(reducers, {}, applyMiddleware(ReduxThunk));
    return (
      <Provider store={store}>
        <Router />
      </Provider>
    );
  }
}
